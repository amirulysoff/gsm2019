<?php
session_start();
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

require('config.php');
require('fpdf.php');

$curr_id=$_GET['id'];
$result = mysqli_query($conn,"SELECT * FROM borang_permohonan where id_borang_permohonan=$curr_id");
$row = mysqli_fetch_array($result);

$title = $row['title'];
$fname = $row['fullname'];
$dob = $row['date_birth'];
$img1 = 'https://gsm.org.my/borang/'.$row['gambarpaspot'];
$img = str_replace(" ", "_", $img1 );
$pob = $row['place_birth'];
$ic = $row['ic_no'];
$nationality = $row['nationality'];
$email = $row['email'];
$phone_off = $row['phone_office'];
$phone_mobile = $row['phone_mobile'];
$addr_home = $row['alamatrumah'];
$addr_off = $row['alamatpejabat'];
$job = $row['pekerjaan'];
$position = $row['jawatan'];
$edu = $row['kelulusan_pelajaran'];
$exp = $row['pengalaman'];
$publisher = $row['penerbitan'];
$note = $row['notes'];
$record = $row['payment_record'];

class PDF extends FPDF
{
// Page header
function Header()
{
    // Logo
    $this->Image('logo.JPG',60,6,80);
    $this->Ln(25);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(30,10,'Membership Application Form',0,0,'C');
    // Line break
    $this->Ln(20);
    //$this->Image('https://gsm.org.my/borang/'.$row['gambarpaspot'],10,6,50);
    //$this->Ln(20);
}

// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

}

// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetFont('Times','',12);

    // gambar pasport
//$pdf->Image('https://gsm.org.my/borang/'.$row['gambarpaspot'],10,6,50);
    //kena letak condition untuk user tak upload gambar
$x = $pdf->GetX();
$y = $pdf->GetY();

$x = 80;
$y = 50;
$pdf->SetXY($x, $y);
$pdf->SetDrawColor(0,0,0);

/*if (file_exists('https://gsm.org.my/borang/'.$row['gambarpaspot'])) {
    $this->Image('https://gsm.org.my/borang/'.$row['gambarpaspot'],25);
} else {
    $this->Image('default_img.png',25);
}*/
$pdf->Cell(35, 45, $pdf->Image($img,80,50,35,0), 1,0); 
$pdf->Ln(60);

//members info start
$pdf->Cell(45   ,5,'Title',0,0);
$pdf->Cell(10   ,5,':',0,0);
$pdf->Cell(140  ,5,$title,0,1);

$pdf->Cell(45   ,5,'Full Name',0,0);
$pdf->Cell(10   ,5,':',0,0);
$pdf->Cell(140  ,5,$fname,0,1);

$pdf->Cell(45   ,5,'Date of Birth',0,0);
$pdf->Cell(10   ,5,':',0,0);
$pdf->Cell(140  ,5,$dob,0,1);


$pdf->Cell(45   ,5,'Place of Birth',0,0);
$pdf->Cell(10   ,5,':',0,0);
$pdf->Cell(140  ,5,$pob,0,1);

$pdf->Cell(45   ,5,'IC/ Passport No',0,0);
$pdf->Cell(10   ,5,':',0,0);
$pdf->Cell(140  ,5,$ic,0,1);

$pdf->Cell(45   ,5,'Nationality',0,0);
$pdf->Cell(10   ,5,':',0,0);
$pdf->Cell(140  ,5,$nationality,0,1);

$pdf->Cell(45   ,5,'Email',0,0);
$pdf->Cell(10   ,5,':',0,0);
$pdf->Cell(140  ,5,$email,0,1);

$pdf->Cell(45   ,5,'Office Number',0,0);
$pdf->Cell(10   ,5,':',0,0);
$pdf->Cell(140  ,5,$phone_off,0,1);

$pdf->Cell(45   ,5,'Mobile Number',0,0);
$pdf->Cell(10   ,5,':',0,0);
$pdf->Cell(140  ,5,$phone_mobile,0,1);

//$text=str_repeat('this is a word wrap test ',20);
//$nb=$pdf->WordWrap($text,220);

//$home_addr = $pdf->WordWrap($row['alamatrumah'],20);

$pdf->Cell(45   ,5,'Home Address',0,0);
$pdf->Cell(10   ,5,':',0,0);
$pdf->MultiCell(140 ,5,$addr_home,0,1);

$pdf->Cell(45   ,5,'Office Address',0,0);
$pdf->Cell(10   ,5,':',0,0);
$pdf->MultiCell(140 ,5,$addr_off,0,1);

/*$pdf->Cell(45   ,5,'Job',0,0);
$pdf->Cell(10   ,5,':',0,0);
$pdf->Cell(140  ,5,$job,0,1);*/

$pdf->Cell(45   ,5,'Occupation',0,0);
$pdf->Cell(10   ,5,':',0,0);
$pdf->Cell(140  ,5,$position,0,1);

//$edu = $pdf->WordWrap($row['kelulusan_pelajaran'],20);
$pdf->Cell(45   ,5,'Education Qualification',0,0);
$pdf->Cell(10   ,5,':',0,0);
$pdf->MultiCell(140 ,5,$edu,0,1);

$pdf->Cell(45   ,5,'Employer',0,0);
$pdf->Cell(10   ,5,':',0,0);
$pdf->MultiCell(140 ,5,$exp,0,1);

$pdf->Cell(45   ,5,'List of Publication',0,0);
$pdf->Cell(10   ,5,':',0,0);
$pdf->MultiCell(140 ,5,$publisher,0,1);

$pdf->Cell(45   ,5,'Date',0,0);
$pdf->Cell(10   ,5,':',0,0);
$pdf->Ln();

$pdf->Cell(45   ,5,'Signature',0,0);
$pdf->Cell(10   ,5,':',0,0);
//$pdf->Cell(140  ,5,$note,0,1);


//members info end



mysqli_close($conn);


$pdf->Output();
?>