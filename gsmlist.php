<?php  
include('config.php'); 
require('fpdf.php');
?>
<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}


// define how many results you want per page
$results_per_page = 30;

// find out the number of results stored in database
$sql='SELECT * FROM borang_permohonan';
$result = mysqli_query($conn, $sql);
$number_of_results = mysqli_num_rows($result);

// determine number of total pages available
$number_of_pages = ceil($number_of_results/$results_per_page);

// determine which page number visitor is currently on
if (!isset($_GET['page'])) {
  $page = 1;
} else {
  $page = $_GET['page'];
}

// determine the sql LIMIT starting number for the results on the displaying page
$this_page_first_result = ($page-1)*$results_per_page;

?>

<html>
<head> 
    <link rel="stylesheet" href="style.css" />
    <title>GSM2019</title>
</head>
<body>

<!--this is main titile-->
      <header>  
      <div id="main_title">
        <h2>Hi, <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b> Welcome to GSM.</h2>
      </div>

        <center><img src="img/gsm.png"></center>

       <center><h2 >GSM 2019</h2></center>

<?php $results = mysqli_query($conn, "SELECT * FROM borang_permohonan"); ?>

<table align="center" border="1">
    <thead>
        <tr>
            <th colspan="2">No.</th>
            <th colspan="2">Name</th>            
            <th colspan="2">IC Number</th>            
            <th colspan="2">Email</th>
            <th colspan="2">Print</th>
        </tr>
    </thead>
    
    <?php 
    $no = 0;
    $sql='SELECT * FROM borang_permohonan ORDER BY fullname  LIMIT ' . $this_page_first_result . ',' .  $results_per_page;
    $resultss = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_array($resultss)) { 
    $result = mysqli_query($conn,"SELECT * FROM borang_permohonan ORDER BY fullname");

//$curr_id = $_GET['id'];
    ?>
<tr>
   <td colspan="2"><?php echo ($no+1)+(($page -1) *30) ?></td> 
   <td colspan="2"><?php echo $row['fullname'] .''?></td>
   <td colspan="2"><?php echo $row['ic_no'] .''?></td>
   <td colspan="2"><?php echo $row['email'] .''?></td>

   <td>
   <a href="print1.php?id=<?php echo $row['id_borang_permohonan']; ?>" value="Print Data" target="_blank" >Print</a>
   </td>
            

            <!--. $row['fullname'].' ' . $row['ic_no'].' ' . $row['email'].  '<br>';
              <tr>   
              <td colspan="2"><?php echo $row['id_borang_permohonan'] .''?></td> 
            <td colspan="2"><?php echo $no++; ?></td>
            <td colspan="2"><?php echo $row['fullname']; ?></td>
            <td colspan="2"><?php echo $row['ic_no']; ?></td>
            <td colspan="2"><?php echo $row['email']; ?></td>
            <td colspan="2">    
            <td colspan="2"><?php echo $row['date_birth']; ?></td>
            <td colspan="2"><?php echo $row['place_birth']; ?></td>
            <td colspan="2"><?php echo $row['nationality']; ?></td>
            <td colspan="2"><?php echo $row['phone_office']; ?></td>
            <td colspan="2"><?php echo $row['phone_mobile']; ?></td>
            <td colspan="2"><?php echo $row['alamatrumah']; ?></td>
            <td colspan="2"><?php echo $row['alamatpejabat']; ?></td>
            <td colspan="2"><?php echo $row['pekerjaan']; ?></td>
            <td colspan="2"><?php echo $row['jawatan']; ?></td>
            <td colspan="2"><?php echo $row['kelulusan_pelajaran']; ?></td>
            <td colspan="2"><?php echo $row['pengalaman']; ?></td>
            <td colspan="2"><?php echo $row['penerbitan']; ?></td>
            <td colspan="2"><?php echo $row['gambarpaspot']; ?></td>
            <td colspan="2"><?php echo $row['pengakuan']; ?></td>-->

        </tr>
    <?php } 
    ?>
</table> 
<center>
<?php
for ($page=1;$page<=$number_of_pages;$page++) {
  echo '<a href="gsmlist.php?page=' . $page . '">' . $page . '</a> ';
}
?>
</center>
</br>
</header>

    <p >
        <a href="logout.php" class="btn btn-danger">Sign Out</a>
    </p>

</body>
</html>
